//
//  ServicesProviderImpl.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import CoreData

class ServicesProviderImpl: ServicesProvider {
    // MARK: Network Data Provider
    private static let apiBaseUrl: URL = URL(string: "https://api.tinkoff.ru/v1/")!

    lazy var networkDataProvider: NetworkDataProvider = {
        let urlSession = URLSession(configuration: .default)
        return NetworkDataProviderImpl(baseUrl: type(of: self).apiBaseUrl, configuration: .default)
    }()

    // MARK: Core Data
    private var _mainManagedObjectContext: NSManagedObjectContext?
    private var saveContextObservingToken: NSObjectProtocol?

    var mainManagedObjectContext: NSManagedObjectContext {
        if (_mainManagedObjectContext == nil) {
            _mainManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            _mainManagedObjectContext!.persistentStoreCoordinator = persistentStoreCoordinator

            saveContextObservingToken = NotificationCenter.default.addObserver(forName: NSNotification.Name.NSManagedObjectContextDidSave, object: nil, queue: OperationQueue.main) { [weak context = self._mainManagedObjectContext] (notification) in
                guard let context = context else {
                    return
                }
                guard let object = notification.object, let savedContext = (object as? NSManagedObjectContext) else {
                    return
                }
                if savedContext != context {
                    context.mergeChanges(fromContextDidSave: notification)
                }
            }
        }
        return _mainManagedObjectContext!
    }

    func makeBackgroundManagedObjectContext() -> NSManagedObjectContext {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = persistentStoreCoordinator

        return context
    }

    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let modelUrl = Bundle.main.url(forResource: "CacheModel", withExtension: "momd")!
        let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl)!

        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)

        let temporatyDirUrl = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        let databaseUrl = temporatyDirUrl.appendingPathComponent("cache_database.sqlite")
        do {
            try coordinator.addPersistentStore(
                ofType: NSSQLiteStoreType,
                configurationName: nil,
                at: databaseUrl,
                options: nil
            )
        } catch let e {
            assertionFailure("persistent store initialization error: \(e)")
        }
        return coordinator
    }()

    //MARK: Error Formatter
    lazy var errorFormatter: ErrorFormatter = ErrorFormatterImpl()
}
