//
//  ServicesProvider.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import CoreData

protocol ServicesProvider {
    var networkDataProvider: NetworkDataProvider { get }

    var mainManagedObjectContext: NSManagedObjectContext { get }
    func makeBackgroundManagedObjectContext() -> NSManagedObjectContext

    var errorFormatter: ErrorFormatter { get }
}
