//
//  NewsViewModel.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

struct NewsViewModelItem {
    let title: String
    let date: String
}

protocol NewsViewModelDelegate: AnyObject {
    func viewModelDidUpdateLoadingState(_ viewModel: NewsViewModel)

    func viewModel(_ viewModel: NewsViewModel, didReceiveError error: String)

    func viewModelDidReloadItems(_ viewModel: NewsViewModel)
    func viewModel(_ viewModel: NewsViewModel, didUpdateItemsAt updatedIndexes: IndexSet, insertedAt insertedIndexes: IndexSet, removedFrom removedIndexed: IndexSet)
}

protocol NewsViewModel: AnyObject {
    weak var delegate: NewsViewModelDelegate? { get set }

    var loading: Bool { get }

    var numberOfItems: Int { get }

    func prepareForUse()

    func refreshData()

    func item(at index: Int) -> NewsViewModelItem

    func selectItem(at index: Int)
}
