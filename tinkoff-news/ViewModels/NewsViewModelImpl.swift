//
//  NewsViewModelImpl.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import CoreData

class NewsViewModelImpl: NSObject, NewsViewModel, NSFetchedResultsControllerDelegate {
    private var fetchedResultController: NSFetchedResultsController<News>?
    private var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium

        return dateFormatter
    }()

    private let servicesProvider: ServicesProvider
    private let router: Router

    deinit {
        activeFetchOperation?.cancel()
    }

    init(servicesProvider: ServicesProvider, router: Router) {
        self.servicesProvider = servicesProvider
        self.router = router
    }

    //MARK: NewsViewModel
    var delegate: NewsViewModelDelegate?

    var numberOfItems: Int {
        guard let controller = fetchedResultController else {
            return 0
        }
        guard let firstSection = controller.sections?.first else {
            return 0
        }
        return firstSection.numberOfObjects
    }

    var loading: Bool = false {
        didSet {
            delegate?.viewModelDidUpdateLoadingState(self)
        }
    }

    func prepareForUse() {
        assert(self.fetchedResultController == nil, "Method should be called once")

        let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: #keyPath(News.publicationDate), ascending: false)
        ]
        let fetchedResultController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: servicesProvider.mainManagedObjectContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        self.fetchedResultController = fetchedResultController
        fetchedResultController.delegate = self
        do {
            try fetchedResultController.performFetch()
        } catch let error as NSError {
            delegate?.viewModel(self, didReceiveError: error.localizedDescription)
        }
        delegate?.viewModelDidReloadItems(self)
        loading = true
        fetchNetworkNews()
    }

    func refreshData() {
        fetchNetworkNews()
    }

    func item(at index: Int) -> NewsViewModelItem {
        assert(index < numberOfItems)

        let indexPath = IndexPath(row: index, section: 0)
        let news = fetchedResultController!.object(at: indexPath)

        let date = news.publicationDate.map { dateFormatter.string(from: $0 as Date) }

        return NewsViewModelItem(
            title: news.text?.replacingHtmlEntities() ?? "",
            date: date ?? ""
        )
    }

    func selectItem(at index: Int) {
        assert(index < numberOfItems)

        let indexPath = IndexPath(row: index, section: 0)
        let news = fetchedResultController!.object(at: indexPath)

        if let identifier = news.identifier {
            router.showDetails(forNewsWithId: identifier)
        }
    }

    //MARK: NSFetchedResultsControllerDelegate

    var updatedIndexes: IndexSet = []
    var insertedIndexed: IndexSet = []
    var removedIndexed: IndexSet = []
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        switch type {
        case .update:
            updatedIndexes.insert(indexPath!.row)
        case .insert:
            insertedIndexed.insert(newIndexPath!.row)
        case .delete:
            removedIndexed.insert(indexPath!.row)
        case.move:
            insertedIndexed.insert(newIndexPath!.row)
            removedIndexed.insert(indexPath!.row)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.viewModel(self, didUpdateItemsAt: updatedIndexes, insertedAt: insertedIndexed, removedFrom: removedIndexed)
        updatedIndexes.removeAll()
        insertedIndexed.removeAll()
        removedIndexed.removeAll()
    }

    //MARK: Private

    private var activeFetchOperation: Cancelable?
    func fetchNetworkNews() {
        if (activeFetchOperation != nil) {
            return
        }
        loading = true
        activeFetchOperation = servicesProvider.networkDataProvider.loadNews { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.activeFetchOperation = nil
            switch result {
            case .success(let newsDto):
                self?.saveNewsDtoToCache(newsDto) { [weak self] in
                    self?.loading = false
                }
            case .failure(let error):
                strongSelf.loading = false
                let errorMessage = strongSelf.servicesProvider.errorFormatter.format(error: error)
                strongSelf.delegate?.viewModel(strongSelf, didReceiveError:errorMessage)
            }
        }
    }

    func saveNewsDtoToCache(_ newsDto: [NewsDto], completeHandler: @escaping () -> Void) {
        let context = servicesProvider.makeBackgroundManagedObjectContext()
        let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
        fetchRequest.fetchLimit = 1

        context.perform {
            for dto in newsDto {
                fetchRequest.predicate = NSPredicate(format: "\(#keyPath(News.identifier)) = %@", dto.id)
                let existObject = try! context.fetch(fetchRequest).first

                let object: News
                if let existObject = existObject {
                    object = existObject
                } else {
                    object = NSEntityDescription.insertNewObject(forEntityName: News.entityName, into: context) as! News
                }
                object.populate(from: dto)
            }
            do {
                try context.save()
            } catch let error as NSError {
                print("context save error: \(error)")
            }
            DispatchQueue.main.async {
                completeHandler()
            }
        }
    }
}
