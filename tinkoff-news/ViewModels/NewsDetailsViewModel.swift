//
//  NewsDetailsViewModel.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

protocol NewsDetailsViewModelDelegate: AnyObject {
    func viewModelDidUpdateLoadingState(_ viewModel: NewsDetailsViewModel)

    func viewModelDidUpdateTitle(_ viewModel: NewsDetailsViewModel)
    func viewModelDidUpdateContent(_ viewModel: NewsDetailsViewModel)

    func viewModel(_ viewModel: NewsDetailsViewModel, didReceiveError error: String)
}

protocol NewsDetailsViewModel: AnyObject {
    weak var delegate: NewsDetailsViewModelDelegate? { get set }

    var loading: Bool { get }

    var title: String? { get }

    var content: String? { get }

    func prepareForUse() -> Void

    func reloadData() -> Void
}
