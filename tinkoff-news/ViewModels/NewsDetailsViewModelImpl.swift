//
//  NewsDetailsViewModelImpl.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import CoreData

class NewsDetailsViewModelImpl: NewsDetailsViewModel {
    private let newsId: String
    private let servicesProvider: ServicesProvider

    private var activeLoadOperation: Cancelable?

    private var news: News? {
        didSet {
            delegate?.viewModelDidUpdateTitle(self)
            delegate?.viewModelDidUpdateContent(self)
        }
    }

    deinit {
        activeLoadOperation?.cancel()
    }

    init(newsId: String, servicesProvider: ServicesProvider) {
        self.newsId = newsId
        self.servicesProvider = servicesProvider
    }

    //MARK: NewsDetailsViewModel
    weak var delegate: NewsDetailsViewModelDelegate?

    var loading: Bool = false {
        didSet {
            delegate?.viewModelDidUpdateLoadingState(self)
        }
    }

    var title: String? {
        return news?.text?.replacingHtmlEntities()
    }

    var content: String? {
        return news?.content
    }

    func prepareForUse() {
        reloadData()
    }

    func reloadData() -> Void {
        news = loadNewsFromCache()
        if content == nil {
            self.loading = true
            self.activeLoadOperation = self.loadNewsDetailsFromNetwork(completeHandler: { [weak self] (error) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.activeLoadOperation = nil
                if let error = error {
                    let errorMessage = strongSelf.servicesProvider.errorFormatter.format(error: error)
                    strongSelf.delegate?.viewModel(strongSelf, didReceiveError:errorMessage)
                } else {
                    strongSelf.news = strongSelf.loadNewsFromCache()
                }
                strongSelf.loading = false
            })
        }
    }

    //MARK: Private
    private func loadNewsFromCache() -> News? {
        let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "\(#keyPath(News.identifier)) = %@", newsId)

        var news: News? = nil
        do {
            news = try servicesProvider.mainManagedObjectContext.fetch(fetchRequest).first
        } catch let error as NSError {
            print("news fetch error: \(error)")
        }
        return news
    }

    private func loadNewsDetailsFromNetwork(completeHandler: @escaping (NetworkDataProviderError?) -> Void) -> Cancelable {
        return servicesProvider.networkDataProvider.loadNewsDetails(withId: newsId) { [weak self] (result) in
            switch result {
            case .success(let dto):
                self?.saveToCacheNewsDetailsDto(dto, completeHandler: {
                    completeHandler(nil)
                })
            case .failure(let error):
                completeHandler(error)
            }
        }
    }

    private func saveToCacheNewsDetailsDto(_ newsDetailsDto: NewsDetailsDto, completeHandler: @escaping () -> Void) {
        let context = servicesProvider.makeBackgroundManagedObjectContext()
        let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
        fetchRequest.fetchLimit = 1

        context.perform {
            fetchRequest.predicate = NSPredicate(format: "\(#keyPath(News.identifier)) = %@", newsDetailsDto.title.id)
            let existObject = try! context.fetch(fetchRequest).first

            let object: News
            if let existObject = existObject {
                object = existObject
            } else {
                object = NSEntityDescription.insertNewObject(forEntityName: News.entityName, into: context) as! News
            }
            object.populate(from: newsDetailsDto)
            do {
                try context.save()
            } catch let error as NSError {
                print("context save error: \(error)")
            }
            DispatchQueue.main.async {
                completeHandler()
            }
        }
    }
}
