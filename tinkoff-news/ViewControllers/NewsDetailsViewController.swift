//
//  NewsDetailsViewController.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import UIKit

class NewsDetailsViewController: UIViewController, NewsDetailsViewModelDelegate {
    @IBOutlet weak var webView: UIWebView!

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!

    private let viewModel: NewsDetailsViewModel

    init(viewModel: NewsDetailsViewModel) {
        self.viewModel = viewModel

        super.init(nibName: "NewsDetailsViewController", bundle: Bundle(for: NewsDetailsViewController.self))
        self.viewModel.delegate = self
        title = NSLocalizedString("news-details.title", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        refreshButton.setTitle(NSLocalizedString("news-details.placeholder.reload-button.title", comment: ""), for: .normal)
        viewModel.prepareForUse()
    }

    //MARK: Actions
    @IBAction func refreshButtonTapped(_ sender: Any) {
        viewModel.reloadData()
    }

    //MARK: NewsDetailsViewModelDelegate
    func viewModelDidUpdateLoadingState(_ viewModel: NewsDetailsViewModel) {
        if viewModel.loading {
            activityIndicatorView.startAnimating()
            placeholderView.isHidden = true
        } else {
            activityIndicatorView.stopAnimating()
        }
    }

    func viewModelDidUpdateTitle(_ viewModel: NewsDetailsViewModel) {
        if let newsTitle = viewModel.title {
            title = newsTitle
        }
    }

    func viewModelDidUpdateContent(_ viewModel: NewsDetailsViewModel) {
        if let content = viewModel.content {
            webView.isHidden = false
            placeholderView.isHidden = true
            webView.loadHTMLString(content, baseURL: URL(string: "https://")!)
        }
    }

    func viewModel(_ viewModel: NewsDetailsViewModel, didReceiveError error: String) {
        if viewModel.content == nil {
            placeholderView.isHidden = false
            placeholderLabel.text = error
        }
    }
}
