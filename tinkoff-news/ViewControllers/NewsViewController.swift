//
//  NewsViewController.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController, NewsViewModelDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet private var placeholderView: UIView!
    @IBOutlet private var placeholderLabel: UILabel!
    @IBOutlet private var reloadButton: UIButton!
    private var refreshControl: UIRefreshControl!

    private let newsCellReuseIdentifier = "NewsCell"

    let viewModel: NewsViewModel

    init(viewModel: NewsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "NewsViewController", bundle: Bundle(for: NewsViewController.self))

        self.viewModel.delegate = self
        title = NSLocalizedString("news.title", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        reloadButton.setTitle(NSLocalizedString("news.placeholder.reload-button.title", comment: ""), for: .normal)

        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlTrigerred(_:)), for: .valueChanged)
        tableView .addSubview(refreshControl)

        let newCellNib = UINib(nibName: "NewsViewCell", bundle: Bundle(for: NewsViewCell.self))
        tableView.register(newCellNib, forCellReuseIdentifier: newsCellReuseIdentifier)

        viewModel.prepareForUse()
    }

    //MARK: Actions
    @IBAction func reloadButtonTapped(_ sender: Any) {
        viewModel.refreshData()
    }

    func refreshControlTrigerred(_ sender: Any) {
        viewModel.refreshData()
    }

    //MARK: NewsViewModelDelegate
    func viewModel(_ viewModel: NewsViewModel, didReceiveError error: String) {
        placeholderView.isHidden = false
        placeholderLabel.text = error
    }

    func viewModelDidUpdateLoadingState(_ viewModel: NewsViewModel) {
        if viewModel.loading {
            activityIndicatorView.startAnimating()
            placeholderView.isHidden = true
        } else {
            refreshControl.endRefreshing()
            activityIndicatorView.stopAnimating()
        }
    }

    func viewModelDidReloadItems(_ viewModel: NewsViewModel) {
        updateViewVisibility()
        tableView.reloadData()
    }

    func viewModel(_ viewModel: NewsViewModel, didUpdateItemsAt updatedIndexes: IndexSet, insertedAt insertedIndexes: IndexSet, removedFrom removedIndexed: IndexSet) {
        updateViewVisibility()
        tableView.beginUpdates()

        let updatedIndexPaths = updatedIndexes.map { IndexPath.init(row: $0, section: 0) }
        tableView.reloadRows(at: updatedIndexPaths, with: .automatic)

        let insertedIndexPaths = insertedIndexes.map { IndexPath.init(row: $0, section: 0) }
        tableView.insertRows(at: insertedIndexPaths, with: .automatic)

        let removedIndexPaths = removedIndexed.map { IndexPath.init(row: $0, section: 0) }
        tableView.deleteRows(at: removedIndexPaths, with: .automatic)

        tableView.endUpdates()
    }

    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = viewModel.item(at: indexPath.row)
        let height = NewsViewCell.height(withItem: item, width: tableView.bounds.width)

        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectItem(at: indexPath.row)
    }

    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel .item(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: newsCellReuseIdentifier, for: indexPath) as! NewsViewCell

        cell.item = item

        return cell
    }

    //MARK: Private
    func updateViewVisibility() {
        let hasItems = (viewModel.numberOfItems > 0)
        tableView.isHidden = !hasItems
        placeholderView.isHidden = hasItems
        placeholderLabel.text = !hasItems ? NSLocalizedString("news.placeholder.label.empty-list-title", comment: "") : nil
    }
}
