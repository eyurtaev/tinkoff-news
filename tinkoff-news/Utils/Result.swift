//
//  Result.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

enum Result<Value, Error: Swift.Error> {
    case success(Value)
    case failure(Error)
}

extension Result {
    func map<T>(_ mapper: (Value) -> T) -> Result<T, Error> {
        switch self {
        case .success(let value):
            return .success(mapper(value))
        case .failure(let error):
            return .failure(error)
        }
    }
}
