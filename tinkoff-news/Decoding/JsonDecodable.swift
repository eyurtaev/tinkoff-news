//
//  JsonDecodable.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

enum JsonDecodingError: Error {
    case incompatibleType(decsription: String?);
    case noRequiredField(decsription: String?);
}

protocol JsonDecodable {
    associatedtype Object

    static func decode(from json: Any) throws -> Object;
}

extension JsonDecodable {
    static func decode(from json: Any, keyPath: String) throws -> Object {
        let keyPathComponents = keyPath.components(separatedBy: ".")
        var targetJson = json
        for component in keyPathComponents {
            guard let dictionary = (targetJson as? Dictionary<String, Any>) else {
                throw JsonDecodingError.incompatibleType(decsription: "expected: Dictionary, actual: \(type(of: targetJson))")
            }
            guard let value = dictionary[component] else {
                throw JsonDecodingError.noRequiredField(decsription: "\(component) key of \(keyPath) key path")
            }
            targetJson = value
        }
        return try Self.decode(from: targetJson)
    }

    static func decodeOptional(from json: Any, keyPath: String) throws -> Object? {
        var value: Object? = nil
        do {
            value = try Self.decode(from: json, keyPath: keyPath)
        } catch JsonDecodingError.noRequiredField(_) {
        } catch (let e) {
            throw e
        }
        return value
    }
}

extension String: JsonDecodable {
    static func decode(from json: Any) throws -> String {
        guard let string = (json as? String) else {
            throw JsonDecodingError.incompatibleType(decsription: "expected: String, actual: \(type(of: json))")
        }
        return string
    }
}

extension Dictionary: JsonDecodable {
    static func decode(from json: Any) throws -> Dictionary<Key, Value> {
        guard let value = (json as? Dictionary<Key, Value>) else {
            throw JsonDecodingError.incompatibleType(decsription: "expected: Dictionary, actual: \(type(of: json))")
        }
        return value
    }
}

extension Int: JsonDecodable {
    static func decode(from json: Any) throws -> Int {
        guard let vaule = (json as? Int) else {
            throw JsonDecodingError.incompatibleType(decsription: "expected: Int, actual: \(type(of: json))")
        }
        return vaule
    }
}

extension Double: JsonDecodable {
    static func decode(from json: Any) throws -> Double {
        guard let vaule = (json as? Double) else {
            throw JsonDecodingError.incompatibleType(decsription: "expected: Double, actual: \(type(of: json))")
        }
        return vaule
    }
}

extension Bool: JsonDecodable {
    static func decode(from json: Any) throws -> Bool {
        guard let value = (json as? Bool) else {
            throw JsonDecodingError.incompatibleType(decsription: "expected: Bool, actual: \(type(of: json))")
        }
        return value
    }
}

func decodeObjects<T: JsonDecodable>(from json: Any) throws -> [T]
where T.Object == T {
    guard let array = (json as? [Any]) else {
        throw JsonDecodingError.incompatibleType(decsription: "expected: Array, actual: \(type(of: json))")
    }
    return try array.map { item in
        return try T.decode(from: item)
    }
}

func decodeUnixtimeDate(from json: Any, keyPath: String) throws -> Date {
    let value = try Double.decode(from: json, keyPath: keyPath)
    let seconds: TimeInterval = (value / 1000)
    return Date(timeIntervalSince1970: seconds)
}
