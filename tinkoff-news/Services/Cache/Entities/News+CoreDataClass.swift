//
//  News+CoreDataClass.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import CoreData


public class News: NSManagedObject {
    static let entityName: String = "News"
}
