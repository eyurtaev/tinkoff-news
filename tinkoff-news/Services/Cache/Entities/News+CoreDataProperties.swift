//
//  News+CoreDataProperties.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import CoreData


extension News {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<News> {
        return NSFetchRequest<News>(entityName: entityName)
    }

    @NSManaged public var identifier: String?
    @NSManaged public var name: String?
    @NSManaged public var text: String?
    @NSManaged public var publicationDate: NSDate?
    @NSManaged public var content: String?
    @NSManaged public var creationDate: NSDate?
    @NSManaged public var lastModificationDate: NSDate?

    func populate(from newsDto: NewsDto) {
        identifier = newsDto.id
        name = newsDto.name
        text = newsDto.text
        publicationDate = newsDto.publicationDate as NSDate
    }

    func populate(from newsDetailsDto: NewsDetailsDto) {
        populate(from: newsDetailsDto.title)
        content = newsDetailsDto.content
        creationDate = newsDetailsDto.creationDate as NSDate
        lastModificationDate = newsDetailsDto.lastModificationDate as NSDate
    }
}
