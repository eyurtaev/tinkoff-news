//
//  NetwordDataProvider.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

enum NetworkDataProviderError: Error {
    case canceled;
    case network;
    case badResponse;
    case api(code: String, description: String?);
}

protocol NetworkDataProvider {
    @discardableResult func loadNews(_ completionHandler: @escaping (Result<[NewsDto], NetworkDataProviderError>) -> Void) -> Cancelable

    @discardableResult func loadNewsDetails(withId id: String, completionHandler: @escaping (Result<NewsDetailsDto, NetworkDataProviderError>) -> Void) -> Cancelable
}
