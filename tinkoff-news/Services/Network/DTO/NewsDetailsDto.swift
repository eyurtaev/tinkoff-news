//
//  NewsDetailsDto.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

struct NewsDetailsDto {
    let title: NewsDto
    let content: String
    let creationDate: Date
    let lastModificationDate: Date
}

extension NewsDetailsDto: JsonDecodable {
    static func decode(from json: Any) throws -> NewsDetailsDto {
        let title = try NewsDto.decode(from: json, keyPath: "title")
        let content = try String.decode(from: json, keyPath: "content")
        let creationDate = try decodeUnixtimeDate(from: json, keyPath: "creationDate.milliseconds")
        let lastModificationDate = try decodeUnixtimeDate(from: json, keyPath: "lastModificationDate.milliseconds")

        return NewsDetailsDto(
            title: title,
            content: content,
            creationDate: creationDate,
            lastModificationDate: lastModificationDate
        )
    }
}
