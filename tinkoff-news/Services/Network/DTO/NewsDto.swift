//
//  NewsDto.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

struct NewsDto {
    let id: String
    let name: String
    let text: String
    let publicationDate: Date
}

extension NewsDto: JsonDecodable {
    static func decode(from json: Any) throws -> NewsDto {
        let id = try String.decode(from: json, keyPath: "id")
        let name = try String.decode(from: json, keyPath: "name")
        let text = try String.decode(from: json, keyPath: "text")
        let publicationDate = try decodeUnixtimeDate(from: json, keyPath: "publicationDate.milliseconds")

        return NewsDto(
            id: id,
            name: name,
            text: text,
            publicationDate: publicationDate
        )
    }
}
