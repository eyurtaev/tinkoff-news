//
//  NetworkDataProviderImpl.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

private struct JsonDecodableArray<Element: JsonDecodable>: JsonDecodable where Element.Object == Element {
    let items: [Element]

    static func decode(from json: Any) throws -> JsonDecodableArray {
        let items: [Element] = try decodeObjects(from: json)
        return JsonDecodableArray(items: items)
    }
}

private struct ApiResponse<Payload: JsonDecodable>: JsonDecodable where Payload.Object == Payload {
    let code: String
    let payload: Payload?
    let errorMessage: String?

    static func decode(from json: Any) throws -> ApiResponse {
        let code = try String.decode(from: json, keyPath: "resultCode")
        let payload = try Payload.decodeOptional(from: json, keyPath: "payload")
        let errorMessage = try String.decodeOptional(from: json, keyPath: "errorMessage")

        return ApiResponse(
            code: code,
            payload: payload,
            errorMessage: errorMessage
        )
    }
}

class NetworkDataProviderImpl: NetworkDataProvider {
    private let baseUrl: URL
    private let urlSession: URLSession

    init(baseUrl: URL, configuration: URLSessionConfiguration) {
        self.baseUrl = baseUrl

        let urlSessionDelegateQueue = OperationQueue()
        urlSessionDelegateQueue.name = "com.test-project.tinkoff-news.network-data-provider.delegate-queue"
        urlSession = URLSession(configuration: configuration, delegate: nil, delegateQueue: urlSessionDelegateQueue)
    }

    // MARK: NetworkDataProvider
    @discardableResult func loadNews(_ completionHandler: @escaping (Result<[NewsDto], NetworkDataProviderError>) -> Void) -> Cancelable {
        return getObject(
            withPath: "news",
            parameters: [:]
        ) { (result: Result<JsonDecodableArray<NewsDto>, NetworkDataProviderError>) in
            let newsResult = result.map { $0.items }
            completionHandler(newsResult)
        }
    }

    @discardableResult func loadNewsDetails(withId id: String, completionHandler: @escaping (Result<NewsDetailsDto, NetworkDataProviderError>) -> Void) -> Cancelable {
        return getObject(
            withPath: "news_content",
            parameters: [
                "id": id,
            ],
            competeHandler: completionHandler
        )
    }

    // MARK: Private
    @discardableResult private func getObject<T: JsonDecodable>(withPath path: String, parameters: [String: String], competeHandler: @escaping (Result<T, NetworkDataProviderError>) -> Void) -> Cancelable
    where T.Object == T {
        var urlComponents = URLComponents(url: URL(fileURLWithPath: path, relativeTo: baseUrl), resolvingAgainstBaseURL: true)!
        urlComponents.queryItems = parameters.map { (key, value) in
            return URLQueryItem(name: key, value: value)
        }
        let url = urlComponents.url!

        let task = urlSession.dataTask(with: url) { (data, response, error) in
            let completeOnMainQueue = { (result: Result<T, NetworkDataProviderError>) in
                DispatchQueue.main.async {
                    competeHandler(result)
                }
            }
            if error != nil {
                completeOnMainQueue(.failure(.network))
                return
            }
            guard let data = data else {
                completeOnMainQueue(.failure(.badResponse))
                return
            }
            let result: Result<T, NetworkDataProviderError> = self.responseObject(from: data)
            completeOnMainQueue(result)
        }
        task.resume()

        return task
    }

    static private let successStatusCode = "OK"
    private func responseObject<T: JsonDecodable>(from data: Data) -> Result<T, NetworkDataProviderError>
    where T.Object == T {
        let apiResponse: ApiResponse<T>
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions())
            apiResponse = try ApiResponse<T>.decode(from: json)
        } catch _ {
            return .failure(.badResponse)
        }

        let result: Result<T, NetworkDataProviderError>
        if apiResponse.code != type(of: self).successStatusCode {
            result = .failure(.api(code: apiResponse.code, description: apiResponse.errorMessage))
        } else if let payload = apiResponse.payload {
            result = .success(payload)
        } else {
            result = .failure(.badResponse)
        }
        return result
    }

}
