//
//  Router.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

protocol Router: AnyObject {
    func showInitial()
    func showDetails(forNewsWithId newsId: String)
}
