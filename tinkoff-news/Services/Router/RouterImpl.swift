//
//  RouterImpl.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import UIKit

class RouterImpl: Router {
    private unowned let navigationController: UINavigationController
    private let servicesProvider: ServicesProvider

    init(navigationCotroller: UINavigationController, servicesProvider: ServicesProvider) {
        self.navigationController = navigationCotroller
        self.servicesProvider = servicesProvider
    }

    //MARK: Router
    func showInitial() {
        let viewModel = NewsViewModelImpl(servicesProvider: servicesProvider, router: self)
        let viewController = NewsViewController(viewModel: viewModel)

        navigationController.setViewControllers([ viewController ], animated: false)
    }

    func showDetails(forNewsWithId newsId: String) {
        let viewModel = NewsDetailsViewModelImpl(newsId: newsId, servicesProvider: servicesProvider)
        let viewController = NewsDetailsViewController(viewModel: viewModel)

        navigationController.pushViewController(viewController, animated: true)
    }
}
