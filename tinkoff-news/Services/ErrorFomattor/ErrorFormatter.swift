//
//  ErrorFormatter.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

protocol ErrorFormatter {
    func format(error: NetworkDataProviderError) -> String
}
