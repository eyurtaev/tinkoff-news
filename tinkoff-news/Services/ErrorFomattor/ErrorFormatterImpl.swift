//
//  ErrorFormatterImpl.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

class ErrorFormatterImpl: ErrorFormatter {
    func format(error: NetworkDataProviderError) -> String {
        let stringRepresentation: String

        switch error {
        case .api(code: _, description: let description) where description != nil:
            stringRepresentation = description!
        case .badResponse:
            stringRepresentation = NSLocalizedString("error.bad-response", comment: "")
        case .network:
            stringRepresentation = NSLocalizedString("error.no-connecion", comment: "")
        default:
            stringRepresentation = NSLocalizedString("error.unknown", comment: "")
        }
        return stringRepresentation
    }
}
