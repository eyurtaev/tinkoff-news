//
//  Cancelable.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 01/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

protocol Cancelable {
    func cancel();
}

extension URLSessionTask: Cancelable {}
