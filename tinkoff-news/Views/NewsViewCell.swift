//
//  NewsViewCell.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import UIKit

class NewsViewCell: UITableViewCell {

    static private let dateLabelFont: UIFont = UIFont.brandRegularFront(ofSize: 14)
    static private let titleLabelFont: UIFont = UIFont.brandRegularFront(ofSize: 17)

    static func height(withItem item: NewsViewModelItem, width: CGFloat) -> CGFloat {
        let contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        let spaceBetweenLabels: CGFloat = 4.0
        let contentWidth = width - contentInset.left - contentInset.right

        let dateLabelBounds = (item.date as NSString).boundingRect(
            with: CGSize(width: CGFloat.infinity, height: CGFloat.infinity),
            options: NSStringDrawingOptions(rawValue: 0),
            attributes: [
                NSFontAttributeName: dateLabelFont,
            ],
            context: nil)
        let titleLabelBounds = (item.title as NSString).boundingRect(
            with: CGSize(width: contentWidth, height: CGFloat.infinity),
            options: .usesLineFragmentOrigin,
            attributes: [
                NSFontAttributeName: titleLabelFont
            ],
            context: nil)

        let height = dateLabelBounds.height + titleLabelBounds.height + contentInset.top + contentInset.bottom + spaceBetweenLabels + 1

        return ceil(height)
    }

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        dateLabel.font = type(of: self).dateLabelFont
        titleLabel.font = type(of: self).titleLabelFont
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        item = nil
    }

    var item: NewsViewModelItem? {
        didSet {
            dateLabel.text = item?.date
            titleLabel.text = item?.title
        }
    }
}
