//
//  StringExtension.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation

extension String {
    func replacingHtmlEntities() -> String {
        var string = self
        string = string.replacingOccurrences(of: "&nbsp;", with: " ")
        string = string.replacingOccurrences(of: "&quot;", with: "\"")
        string = string.replacingOccurrences(of: "&lt;", with: "<")
        string = string.replacingOccurrences(of: "&gt;", with: ">")

        return string
    }
}
