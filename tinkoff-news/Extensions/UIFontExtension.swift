//
//  UIFontExtension.swift
//  tinkoff-news
//
//  Created by Evgeniy Yurtaev on 02/07/2017.
//  Copyright © 2017 Evgeniy Yurtaev. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static func brandRegularFront(ofSize size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
}
